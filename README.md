vVoteCA
=======

vVoteCA is a set of scripts and utilities for managing an OpenSSL based CA. This repository includes the scripts for setting up a new CA structure, as well as a working development set of CAs that can be used for signing and revoking certificates during development. The development CA should not be used for any production work - the private keys are held within this repository and passwords given below, as such, they should be assumed to be in the public domain.

Setting up a new CA
===================
The createCA.sh script provides the functionality to setup a root CA and several sub-CAs. If you wish to change the names or CAs to be created you should edit the file. Look for the last few lines and you should see
```
createCAStructure "vVoteRootCA"
createRootCA "vVoteRootCA"
createCAStructure "PeerCA"
createSubCA "PeerCA" "vVoteRootCA"
createCAStructure "EBMCA"
createSubCA "EBMCA" "vVoteRootCA"
createCAStructure "VPSCA"
createSubCA "VPSCA" "vVoteRootCA"
createCAStructure "MixServerCA"
createSubCA "MixServerCA" "vVoteRootCA"
```

This is where the actual work is done. The first line creates a CAStructure (files, folders and scripts) for a CA called vVoteRootCA. The second line `createRootCA "vVoteRootCA"` creates a root CA in the specified folder - which should be the same as the name used previously to create the structure.

Following that the same approach is used for each sub CA. Firstly create the structure `createCAStructure "PeerCA"` and then create the subCA `createSubCA "PeerCA" "vVoteRootCA"` specifying the name of the parent CA.

To add additional sub CAs add additional pairings of `createCAStructure` and `createSubCA`, to remove a sub CA from creating delete the pairing.

This is a one time setup that should not be repeated on the same folder structure. During the setup you will be asked for passwords to create the various keys, requests and certificates. Once this has completed the CA hierarchy will have been setup and ready to use.


Signing a CSR
=============
To sign a CSR firstly navigate to the appropriate sub CA - you should not sign CSRs with the root CA. Having navigated to the chosen sub CA copy the CSR into the `certreqs` folder. Then call `./signCSR.sh` follow the instructions and at the end all awaiting CSRs will have been signed and the certificates placed in the `issued` folder. The generated certificate can be copied from the issued folder and sent back to the requesting device. It is sensible to leave the generated certificate in the issued folder, however, a backup copy is also stored in certs.db.

Suspending a Certificate
========================
Suspending a certificate is a particular form of revocation that is reversible. It should only be used in cases where the keys are still secure, but the certificate is not currently considered active - for example staged but not deployed devices.

The suspend a certificate call `./suspendCert.sh [pathToCert]` whereby the pathToCert is the path to the certificate to suspend. Normally, this certificate will be in the issued folder, for example, `./suspendCert.sh ./issued/newdevicecert.pem` will suspend the certificate in newdevicecert.pem.

Having suspended a certificate it will be marked as revoked and will not be accepted. However, a new Certificate Revocation List MUST be generated and distributed before the suspension will come into force. See later section on certificate revocation.

Reinstate a suspended Certificate
=================================
If a certificate has been suspended it can be reinstated. This should only be done if the keys are known to be have been kept secure and there is no longer any reason to have the certificate on hold.

To reinstate a certificate call `./reinstateCert.sh [pathToCert]`. This makes a call to the CA.jar to edit the index.txt to manually reinstate the certificate. This manual reinstatement is required because the OpenSSL CA tools does not provide the necessary functionality to perform this action. Having reinstated the certificate you will need to regenerate the CRL again. The reinstatement will only become active once the updated CRL has been distributed.

Revoke a Certificate
====================
If a certificate is to be permanently revoked, for example, if a device was stolen, you can use the revoke script. Note: you cannot unrevoke a certificate.

To revoke a certificate call `./revokeCert.sh [pathToCert]` whereby the pathToCert is the path to the certificate to revoke. Again, a new Certificate Revocation List MUST be generated and distributed before the suspension will come into force. See later section on certificate revocation lists.

Generate a Certificate Revocation List
======================================
To generate a CRL call `genCRL.sh` and follow the instructions. This will create a new CRL file, backing up the old one first, in the directory crldb. The generate CRL can be found in the subCA directory with a filename of the subCA name and a .crl extension. This file should be distributed to any interested parties.

The generated CRL will have a lifetime 60 days because of the limited changes we expect and the issue of manual distribution to the running peers.

* * *

Mirror CA - Development CA
==========================

The Mirror CA is a mirror of the current development CA that has been partially automatically setup and partially manually setup. It was initially setup using the above scripts before the keys were manually changed to be the existing private keys used in the gnoMint CA. This allows existing devices to be used alongside new devices in development without having to change all the SSL certificates. This has also allowed the gradual development and deployment CRL checking on the peers. 

The Mirror CA consists of a root CA called `SurreyPAV` and the following SubCAs:

+ EBMCA
+ VPSCA
+ MixServerCA
+ PeerCA

Those CAs have the same functionality as described above. Passwords for the CA and all sub CAs is `password`. 

**WARNING: Do not use the Mirror CA for any production functions - all the keys and passwords are provided in this repository for development purposes**


