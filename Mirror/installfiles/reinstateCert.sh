#!/bin/bash

echo "About to Re-Instate $1"
java -jar CA.jar reinstate $1
if [ "$?" -ne "0" ]; then
	echo "Re-instate failed"
else
	echo "Re-instated $1"
	echo "You must now create a new CRL!"
fi



