#!/bin/bash
CA_DIR="VVoteRootCA"
CERTS_DIR="certsdb"
CRL_DIR="crldb"
CERT_REQS="certreqs"
PRIVATE_DIR="private"
ISSUED_DIR="issued"
PROCESSED_CSRS="processed"
INSTALL_FILES="installfiles"

function createCAStructure() {
	echo "Creating directory structure for $1"
  if [ -d "$1" ]; then
  	echo "VVoteCA Directory already exists - cannot continue"
		exit
	fi
	mkdir -p "$1"
	mkdir -p "$1/$CERTS_DIR"
	mkdir -p "$1/$CERT_REQS"
	mkdir -p "$1/$CRL_DIR"
	mkdir -p "$1/$PRIVATE_DIR"
	mkdir -p "$1/$ISSUED_DIR"
	mkdir -p "$1/$PROCESSED_CSRS"
  chmod 700 "$1/$PRIVATE_DIR"
  touch "$1/index.txt"
	echo 01 > "$1/crlnumber"
  cp "$INSTALL_FILES/rootca.conf" "$1/ca.conf"
  cp "$INSTALL_FILES/signCSR.sh" "$1/"
  cp "$INSTALL_FILES/revokeCert.sh" "$1/"
  cp "$INSTALL_FILES/genCRL.sh" "$1/"
	echo "Finished creating directory structure for $1"  
}
function createRootCA() {
	echo "Creating Root CA - $1"
	read -s -p "Enter RootCA($1) Password: " rootCAPassword
  cd $1
	echo "Creating $1 KeyPair"
	openssl req -new -newkey rsa:5120 -keyout "./$PRIVATE_DIR/cakey.pem" -subj "/C=AU/ST=Victoria/L=Melbourne/O=VEC/OU=vVote/CN=vVoteRootCA" -passout pass:$rootCAPassword -out "./careq.pem" -config "./ca.conf" 
  openssl ca -batch -notext -create_serial -out "./cacert.pem" -days 365 -keyfile "./$PRIVATE_DIR/cakey.pem" -passin pass:$rootCAPassword -selfsign -extensions v3_ca -config "./ca.conf" -infiles "./careq.pem" 
  openssl ca -batch -config ca.conf -passin pass:$rootCAPassword -gencrl -out "./$1.crl"
	cd ..
}
function createSubCA() {
	local subCAPassword=""
	local subCA=$1
	local rootCA=$2
	echo "Creating Sub CA - $subCA"
	read -s -p "Enter SubCA($1) Password: " subCAPassword
  cd $subCA
	echo "Creating $subCA KeyPair"
	openssl req -new -newkey rsa:5120 -keyout "./$PRIVATE_DIR/cakey.pem" -subj "/C=AU/ST=Victoria/L=Melbourne/O=VEC/OU=vVote/CN=$subCA" -passout pass:$subCAPassword -out "./${subCA}_careq.pem" -config "./ca.conf" 
	cp "./${subCA}_careq.pem" "../$rootCA/$CERT_REQS/"
	cd ..
	cd $rootCA
  openssl ca -batch -notext -days 364 -keyfile "./$PRIVATE_DIR/cakey.pem" -out "./$ISSUED_DIR/${subCA}cert.pem" -passin pass:$rootCAPassword -extensions v3_ca -config "./ca.conf" -infiles "./$CERT_REQS/${subCA}_careq.pem" 
	cp "./$ISSUED_DIR/${subCA}cert.pem" "../$subCA/cacert.pem"
	cd ..
	cd $subCA
  openssl ca -batch -config ca.conf -passin pass:$subCAPassword -gencrl -out "./$1.crl"
  cd ..
}
createCAStructure "SurreyPAV"
createRootCA "SurreyPAV"
createCAStructure "PeerCA"
createSubCA "PeerCA" "SurreyPAV"
createCAStructure "EBMCA"
createSubCA "EBMCA" "SurreyPAV"
createCAStructure "VPSCA"
createSubCA "VPSCA" "SurreyPAV"
createCAStructure "MixServerCA"
createSubCA "MixServerCA" "SurreyPAV"




