#!/bin/bash
read -s -p "Enter SubCA Password: " subCAPassword
echo "About to generate CRL"
cp  --backup=t "./crl.pem" "./crldb/"
openssl ca -batch -config ca.conf -passin pass:$subCAPassword -gencrl -out "./crl.pem"
if [ "$?" -ne "0" ]; then
	echo "CRL Generation failed"
else
	echo "CRL Generated"
fi



