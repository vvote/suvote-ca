#!/bin/bash
read -s -p "Enter SubCA Password: " subCAPassword
echo "About to revoke $1"
openssl ca -config ca.conf -passin pass:$subCAPassword -revoke $1
if [ "$?" -ne "0" ]; then
	echo "Revocation failed"
else
	echo "Revoked $1"
	echo "You must now create a new CRL!"
fi



